import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Client, Events, GatewayIntentBits } from "discord.js";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.PORT);

  const client = new Client({ intents: [GatewayIntentBits.Guilds] });
  client.once(Events.ClientReady, c => {
    console.log(`Ready! Logged in as ${c.user.tag}`);
  });
  client.login(process.env.BOT_TOKEN);
  client.on(Events.VoiceStateUpdate, () => {
    console.log('voice update');
  })
}
bootstrap();
